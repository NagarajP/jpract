package com.myzee.pract;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DoubleBraceInitialization {

	public static void main(String[] args) {

		/*
		 * In the below example, we see that the hm were initialized by 
		 * using double braces.
		 * The first brace does the task of creating an anonymous inner class that has 
		 * the capability of accessing the parent class�s behavior. In our example, 
		 * we are creating the subclass of HashMap so that it can use the put() method of HashSet.
		 * The second braces do the task of initializing the instances.
		 */
		Map<Integer, Integer> hm = new HashMap<Integer, Integer>() {
			{
				put(1, 1);
				put(2, 2);
				put(3, 3);
			}
		};
		
		hm.forEach((k, v) -> System.out.println(k + " - " + v));
		
		
		/*
		 * In the below example, we see that the stringSet were initialized by using double braces.
		 * The first brace does the task of creating an anonymous inner class that has the 
		 * capability of accessing the parent class�s behavior. 
		 * In our example, we are creating the subclass of HashSet so that it can use the add() method of HashSet.
		 * The second braces do the task of initializing the instances.
		 */
		Set<String> stringSet = new HashSet<String>() {
			{
				add("one");
				add("two");
				add("three");
			}
		};
		stringSet.forEach(System.out::println);
	}

}

package com.myzee.pract;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockInterfaceDemo {

	public static void main(String[] args) {
		HiObject hi = new HiObject("hello");
		NaggThread t1 = new NaggThread(hi);
		t1.start();

		NaggThread t2 = new NaggThread(hi);
		t2.start();
	}
}

class NaggThread extends Thread {
	HiObject hiobj;

	public NaggThread(HiObject hiobj) {
		// TODO Auto-generated constructor stub
		this.hiobj = hiobj;
	}

	@Override
	public void run() {
		super.run();
		hiobj.call();
	}
}

class HiObject {
	String msg;
	Lock lock = new ReentrantLock();

	HiObject(String msg) {
		this.msg = msg;
	}

	public void call() {
		
		lock.lock();
		for (int i = 0; i < 20; i++) {
			System.out.println(Thread.currentThread().getName() + " - " + this.msg);
		}
		lock.unlock();
		
		//or 
		
//		synchronized (this) {
//			for (int i = 0; i < 20; i++) {
//				System.out.println(Thread.currentThread().getName() + " - " + this.msg);
//			}
//		}
	}

}

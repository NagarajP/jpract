package com.myzee.pract;

public class ConstructorChaining {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Derived1 d = new Derived1(10);
	}

}

class Base1 {
	static {
		System.out.println("base : first static block");
	}
	{
		System.out.println("base: first init block");
	}
	Base1() {
		System.out.println("Def constructor : base");
	}
}

class Derived1 extends Base1 {
	static {
		System.out.println("derived : first static block");
	}
	{
		System.out.println("derived : first init block");
	}
	Derived1() {
		System.out.println("def const : derived");
	}
	Derived1(int a) {
		this();
		System.out.println("one arg const : derived");
	}
}

package com.myzee.pract;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class TypesOfCreatingObjects {

	public static void main(String[] args) {

		/* 
		 * Using new keyword 
		 */
		CreateObject co = new CreateObject();
		co.show("new()");
		
		/* 
		 * Using class.forname and class.newInstance 
		 */
		
		try {
			Class cls = Class.forName("com.myzee.pract.CreateObject");
			CreateObject co1 = (CreateObject) cls.newInstance();
			co1.show("newInstance");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 *  Using clone() method 
		 * 
		 */
		CloneExample cloneObj = new CloneExample();
		try {
			CloneExample ce1 = (CloneExample) cloneObj.clone();
			ce1.show();
			
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * Using Deserialization process, JVM creates an object
		 */
		System.out.println("\nUsing Deserialization process, JVM creates an object\n");
		try {
			DeserializeObject dobj = new DeserializeObject("nagg");
			FileOutputStream fos = new FileOutputStream("deser.txt");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(dobj);
			
			DeserializeObject sobj;
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("deser.txt"));
			sobj = (DeserializeObject) ois.readObject();
			System.out.println(sobj.toString());
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}

class CreateObject {
	public void show(String keyWord) {
		System.out.println("CreateObject::show() called, using " + keyWord + " keyword");
	}
}

class CloneExample implements Cloneable{
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	
	public void show() {
		System.out.println("CloneExample::show() called");
	}
}

class DeserializeObject implements Serializable {
	String name;
	public DeserializeObject(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.name;
	}
}

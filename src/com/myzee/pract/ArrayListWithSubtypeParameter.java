package com.myzee.pract;

import java.util.ArrayList;

public class ArrayListWithSubtypeParameter {

	public static void main(String[] args) {
		// you cannot refer A type to B using arraylist
		ArrayList<A> arr = new ArrayList<B>();
	}

}

class A {
	
}

class B extends A {
	
}
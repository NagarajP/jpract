package com.myzee.pract;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class IteratorTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> l = new ArrayList<>();
		l.add(19);
		l.add(4);
		l.add(7);
		l.add(9);
		l.add(8);
		
		Iterator itr = l.iterator();
		while (itr.hasNext()) {
			int i = (int) itr.next();
			if (i == 7) {
				itr.remove();
			} else {
				//l.add(6);//excepiont: concurrentmodificationexception
			}
		}
		System.out.println(l);
		
//		l.iterator().forEachRemaining(e -> System.out.println(e));
		l.listIterator().forEachRemaining(e -> {
			if(e == 9) {
				l.set(0, 89);
			}
		});
		System.out.println(" java8 listiterator: " + l);
		
//		@SuppressWarnings("rawtypes")
		ListIterator litr = l.listIterator();
		while (litr.hasNext()) {
			int i = (int) litr.next();
			if (i == 8) {
				l.set(0, 1);
			}
		}
		System.out.println(l);
	}

}

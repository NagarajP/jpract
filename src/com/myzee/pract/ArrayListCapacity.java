package com.myzee.pract;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/*
 * ArrayList capacity can only be obtained using reflection, no utility method 
 * has provided to get it.
 * ArrayList increases the capacity to 50% when its reached to its initial 
 * capacity. 
 * Default capacity is 10
 */
public class ArrayListCapacity {

	public static void main(String[] args) {
		List<Integer> intList = new ArrayList<>();
		int capacity;
		try {
			intList.add(1);
			capacity = getCapacity(intList);
			System.out.println("capacity = " + capacity);
			
			intList.add(2);
			intList.add(3);
			intList.add(4);
			intList.add(5);
			intList.add(5);
			intList.add(5);
			intList.add(5);
			intList.add(5);
			intList.add(5);
			intList.add(5);
			capacity = getCapacity(intList);
			System.out.println("capacity = " + capacity);
			
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static int getCapacity(List<Integer> intList) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field field = ArrayList.class.getDeclaredField("elementData");
		field.setAccessible(true);
		int capacity = ((Object[])field.get(intList)).length;
		return capacity;
	}

}

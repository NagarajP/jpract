package com.myzee.pract;

public class BreakAsGotoInJava {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		first: {
//		System.out.println("in first block");
		second: {
			System.out.println("in second block");
			break first;
//			System.out.println("after break");
		}
//		System.out.println("after second block");
	}
	System.out.println("after first block");
	}

}

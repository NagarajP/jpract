package com.myzee.pract;

class Test1 { 

	static int i; 
	int j; 
	static { 
		i = 10; 
		System.out.println("static block called "); 
	} 
	Test1(){ 
		System.out.println("Constructor called"); 
	} 
} 

class Main { 
	public static void main(String args[]) { 

	// Although we have two objects, static block is executed only once. 
	//Test t1 = new Test(); 
	//Test t2 = new Test(); 
      try {
		Class.forName("com.myzee.pract.Test1");
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	} 
} 

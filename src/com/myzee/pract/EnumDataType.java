package com.myzee.pract;
/*
 * Notice the 'enum' keyword instead of 'class' keyword.
 * enums can be written inside class, outside class not inside the methods.
 */

// Color is written outside the class
enum Color {
	RED, GREEN, PINK
}

class EnumDataType {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Color c1 = Color.RED;
		System.out.println("Color is : " + c1);
		EnumInsideClass e = new EnumInsideClass();
		e.ShowShape();
	}
	File file = new File("C:\\drive\\prashant\\aa.jpg");
}

class EnumInsideClass {
	
	//Shape is written inside the class
	enum Shape{
		SQUARE, TRIANGLE, RECTANGLE
	}
	public void ShowShape() {
		System.out.println("Shape is : " + Shape.TRIANGLE);
		
	}
}

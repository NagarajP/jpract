package com.myzee.pract;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class ByteBufferTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RandomAccessFile aFile = null;
		try {
			aFile = new RandomAccessFile("a.txt", "rw");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FileChannel chan = aFile.getChannel();
		//Creating buffer
		ByteBuffer buff = ByteBuffer.allocate(66);
		
		//read the file contents into buffer
		int fileBytes = 0;
		try {
			fileBytes = chan.read(buff);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		while (fileBytes != -1) {
			buff.flip();
			
			while (buff.hasRemaining()) {
				System.out.println((char)buff.get());
			}
			buff.clear();
			try {
				fileBytes = chan.read(buff);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}

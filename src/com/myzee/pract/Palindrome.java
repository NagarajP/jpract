package com.myzee.pract;

public class Palindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = "nagarajeejaragan";
		int len = s.length();
		int last = len;
		int flag = 0;
		for (int i = 0; i < len/2; i ++) {
			Character a = s.charAt(i);
			Character b = s.charAt(--last);
//			System.out.println(a);
//			System.out.println(b);
			if (!a.equals(b)) {
				flag = -1;
				break;
			}
		}
		
		if (flag == 0) {
			System.out.println(s + " is palindrome");
		} else {
			System.out.println("not palindrome");
		}
	}

}

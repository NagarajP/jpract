package com.myzee.pract;

public class ObjectSwap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car c1 = new Car("Honda-WRV", "8432");
		Car c2 = new Car("Hundai-Verna", "xxxx");
		CarWrapper cw1 = new CarWrapper(c1);
		CarWrapper cw2 = new CarWrapper(c2);
		
		System.out.println("Before swapping - ");
		cw1.showCarDetails();
		cw2.showCarDetails();
		swap(cw1, cw2);
		System.out.println("After swapping cars using CarWrapper calss");
		cw1.showCarDetails();
		cw2.showCarDetails();
	}

	private static void swap(CarWrapper cw1, CarWrapper cw2) {
		// TODO Auto-generated method stub
		Car temp = cw1.c;
		cw1.c = cw2.c;
		cw2.c = temp;
	}
}

class Car {
	String model, number;
	public Car(String model, String number) {
		this.model = model;
		this.number = number;
	}
}

class CarWrapper {
	Car c;
	public CarWrapper( Car c) {
		this.c = c;
	}
	
	public void showCarDetails() {
		System.out.println("model - " + this.c.model + " number - " + this.c.number);
	}
}


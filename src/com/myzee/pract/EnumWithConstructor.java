package com.myzee.pract;

import java.util.List;

public class EnumWithConstructor {

	public static void main(String[] args) {
		
//		IPO.TEGA.setName("TGEIND");
		IPO[] e = IPO.values();
		for(IPO ipo: e) {
			System.out.println("ipo name : " + ipo.getName() + " & price : " + ipo.getPriceBand());
		}
		
		//System.out.println(IPO.TEGA.getName());

	}

}

enum IPO {
	//constructor is mandatory to create enum with parameters
	HI, TEGA("Tega ind", 453), MAPMYINDIA("CE info systems", 1033), ARWL("Anand Rathi", 550);

	
	private String name;
	private int priceBand;

	// parameterized constructor
	IPO(String name, int i) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.priceBand = i;
	}

	// default/no param constructor
	IPO() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPriceBand() {
		return priceBand;
	}

	public void setPriceBand(int priceBand) {
		this.priceBand = priceBand;
	}
	
	
}
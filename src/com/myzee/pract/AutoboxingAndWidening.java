package com.myzee.pract;

public class AutoboxingAndWidening {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/*
		 * Autoboxing: conversion between primitive type to corresponding wrapper class object. ex: int to Integer, double to Double.
		 */
		method_autoboxing(10);
		method_autoboxing(new Integer(10));
		
		/*
		 * Widening : Convertion between lower capacity primitive to higher capacity primitive. ex: int to float
		 */
		method_widening(25);
		
	}
	
	private static void method_autoboxing(Integer i) {
		System.out.println("autoboxing : int to integer");
	}
	
	/*
	 * private static void method_autoboxing(int i) {
	 * System.out.println("accepts direct int value as input parameter"); }
	 */
	
	private static void method_widening(float i) {
		System.out.println("input widening");
	}

}

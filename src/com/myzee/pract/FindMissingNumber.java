package com.myzee.pract;

import java.util.Arrays;
import java.util.IntSummaryStatistics;

/*
 * Given an array of non-duplicating numbers from 1 to n where one number 
 * is missing, write an efficient java program to find that missing number.
 */
public class FindMissingNumber {

	public static void main(String[] args) {
		int n = 20;
		int[] a = {1,2,3,4,5,6,7,8,9,10,11,12,13,15,16,17,18,19,20};
		
		IntSummaryStatistics iss = new IntSummaryStatistics();
		Arrays.stream(a).forEach(i -> iss.accept(i));
		int sum = (int) iss.getSum();

		int sumToN = n * (n+1)/2;
		
		System.out.println("missing number is = " + (sumToN - sum));
	}

}

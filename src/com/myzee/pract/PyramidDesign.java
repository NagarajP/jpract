package com.myzee.pract;

public class PyramidDesign {

	public static void main(String[] args) {
		
		int n = 10;
		int count = 1;
		for (int i = n; i >=1; i--) {
			
			//for printing spaces
			for (int j = 1; j <= (i)-1; j++) {
				System.out.print(" ");
			}
			
			for (int k = count; k >=1; k--) {
				System.out.print("* ");
			}
			
			System.out.println();
			count++;
		}
	}

}

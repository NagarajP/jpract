package com.myzee.pract;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Immutable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		SubEmp s = (SubEmp) new Employee();
//		s.subEmp();
		HashMap<String,String> map = new HashMap<>();
		map.put("unicorn", "ka02");
		Employee e = new Employee("xyz" , 12,map);
		System.out.println(e.getVehiclesMap());
		
		 e.getVehiclesMap().put("unicorn", "abc");
			System.out.println(e.getVehiclesMap());

	}

}

final class Employee {
	final String name;
	final int id;
	final Map<String , String > vehiclesMap;
	
	public Employee(String name , int id , Map<String , String> map) {
		this.name = name;
		this.id = id;
/*
		Map<String, String> tmap = new HashMap<>();
		for(Entry<String, String> e : map.entrySet() ) {
			tmap.put(e.getKey(), e.getValue());
		}
		this.vehiclesMap = tmap;
*/
		//or using java8
		vehiclesMap = map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
	
	public String getName() {
		return this.name;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the vehiclesMap
	 */
	public HashMap<String, String> getVehiclesMap() {
		return (HashMap<String, String>) this.vehiclesMap.clone();
	}
}


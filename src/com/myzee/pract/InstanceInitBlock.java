package com.myzee.pract;

public class InstanceInitBlock {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Derived d = new Derived();
		
	}
}

class Base {
	public Base() {
		System.out.println("Base constructor called");
	}
	{
		System.out.println("Base instance initializer block");
	}
}

class Derived extends Base {
	public Derived() {
		System.out.println("Derived constructor called");
	}
	{
		System.out.println("Derived instance initializer block");
	}
}

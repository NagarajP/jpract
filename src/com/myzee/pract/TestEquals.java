package com.myzee.pract;

public class TestEquals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Document d1 = new Document(1, "doc1");
		Document d2 = new Document(1, "doc1");
		System.out.println(d1==d2);
		System.out.println(d1.equals(d2));
		
		Integer i = new Integer(10);
		Integer j = new Integer(10);
		System.out.println(i==j);
		System.out.println(i.equals(j));
		
	}
}

class Document {
	int id;
	String name;
	Document(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return this.id == ((Document)obj).id && this.name.equals(((Document)obj).name);
	}
}

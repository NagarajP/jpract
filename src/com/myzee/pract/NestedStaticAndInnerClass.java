package com.myzee.pract;

public class NestedStaticAndInnerClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		OuterClass.NestedStaticClass staticObj = new OuterClass.NestedStaticClass();
		staticObj.run();
		System.out.println(staticObj.nest);
		
		OuterClass out = new OuterClass();
		OuterClass.InnerClass oi = out.new InnerClass();
		oi.run();
		System.out.println(oi.in);
	}
}

class OuterClass {
	static int out = 0;
	
	public static class NestedStaticClass {
		int nest = 10;
		
		public void run() {
			System.out.println("Inside nested static class" + out);
		}
	}
	
	public class InnerClass {
		int in = 20;
		public void run() {
			System.out.println("Inside inner class(non-static class)" + out);
		}
	}
}

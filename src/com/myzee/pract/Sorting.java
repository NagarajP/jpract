package com.myzee.pract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Sorting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> al = new ArrayList<>();
		al.add(1);
		al.add(9);
		al.add(2);
		al.add(0);
//		al.add('a');
//		al.add('n');
		System.out.println(al);
//		System.out.println(Collections.sort(al));
//		Collections.sort(al);
//		System.out.println(al);
		
		// Using java8 streams and functional interface and sort
		al.sort((a,b) -> {
			return a.compareTo(b);
		});
		
		System.out.println(al);
		
		//reverse order
		al.sort((a,b) -> {
			return b.compareTo(a);
		});
		
		System.out.println(al);
	}

}

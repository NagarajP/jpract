package com.myzee.pract;

import java.util.Arrays;
import java.util.Iterator;

public class ReverseStringBySwap {

	public static void main(String[] args) {
		String str = "Nagaraj";
		char[] strCharArray = str.toCharArray();
		reverse(strCharArray);
	}

	private static void reverse(char[] strCharArray) {
		for(int i = 0; i <= (strCharArray.length - 1)/2; i++) {
			swap(strCharArray, i, strCharArray.length - i - 1);
		}
		
		for(char c : strCharArray) {
			System.out.println(c);
		}
	}

	private static void swap(char[] strCharArray, int i, int j) {
		char temp = strCharArray[i];
		strCharArray[i] = strCharArray[j];
		strCharArray[j] = temp;
	}

}

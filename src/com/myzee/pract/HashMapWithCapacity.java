package com.myzee.pract;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HashMapWithCapacity {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<Integer, String> m = new HashMap<>(10);
		m.put(1, "one");
		m.put(2, "two");
		m.put(3, "three");
		m.put(4, "four");
		
		Iterator<Integer> itr = m.keySet().iterator();
		int i = 0;
		while(itr.hasNext()) {
			System.out.println(i++ + " element is - " + itr.next());
//			System.out.println(m.hashCode());
		}
		
		System.out.println(m.size());
	}
}

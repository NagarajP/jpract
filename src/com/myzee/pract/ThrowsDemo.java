package com.myzee.pract;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ThrowsDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			testThrowsCall();
			System.out.println("after test call");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("handle in catch");
		}
	}
	
	private static void testThrowsCall() throws IOException{
		File f = new File("aaaa.txt");
		System.out.println("----");
		FileInputStream fis = new FileInputStream(f);
		System.out.println("after fis");
	}

}

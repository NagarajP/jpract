
/*
 * Final classes cannot be inherited.
 */
package com.myzee.pract;

public class Final {
//	 final String str;
	String str;

	public Final(String s) {
		// TODO Auto-generated constructor stub
		this.str = s;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final Final a = new Final("Hello");
		System.out.println(a);
		a.str = "welcome"; // as str variable is not final inside Final class.
		System.out.println(a);
		Final b = new Final("Hi");
		b.str = "hello";
		a = b;

	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return str;
	}
}
